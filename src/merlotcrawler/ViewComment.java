/*
=============
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package merlotcrawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Gets information from http://www.merlot.org/merlot/viewComment.htm?id=
 * @author Victor Apellaniz, adapted by Henrique Lemos (henriquelds94@gmail.com)
 */
public class ViewComment {
    private String _url;
    private String _code;

    private int _idComment;
    private String _material;
    private int _materialID;
    private int _rating;
    private boolean _classroomUse;
    private String _writtenBy;
    private int _authorID;
    private Date _dateAdded;
    private String _remarks;
    private String _technicalRemarks;

    /**
     * Constructor
     * @param id
     * @param material
     */
    public ViewComment(int id,String material)
    {
        _url="https://www.merlot.org/merlot/viewComment.htm?id=" + id;
        _code=getCodeWebPage(_url);

       _idComment=id;
       //System.out.println("_idComment="+_idComment);
       _material=material;
       //System.out.println("_material="+_material);
       _rating=extractRating("starApricot.gif");
       //System.out.println("_rating="+_rating);
       _classroomUse=extractClassroomUse();
       //System.out.println("_classroomUse="+_classroomUse);
       _writtenBy=extractAuthor();
       //System.out.println("_writtenBy="+_writtenBy);
       _authorID=extractAuthorID();
       //System.out.println("_authorID="+_authorID);
       _dateAdded=extractDateAdded();
       //System.out.println("_dateAdded="+_dateAdded);
       _remarks=extractRemarks();
       //System.out.println("_remarks="+_remarks);
       _technicalRemarks=extractTechnicalRemarks();
       //System.out.println("_technicalRemarks="+_technicalRemarks);
    }
    public ViewComment(int id) throws IOException
    {
        _url="https://www.merlot.org/merlot/viewComment.htm?id=" + id;
        _code=getCodeWebPage(_url);
        /*FileWriter fw = new FileWriter(new File("C:\\Users\\Henrique\\Desktop\\page.html"));
        fw.append(_code);
        fw.close();*/
       _idComment=id;
       //System.out.println("_idComment="+_idComment);
       _materialID=extractMaterialID();
       //System.out.println("_materialID="+_materialID);
       _material=extractMaterial();
       //System.out.println("_material="+_material);
       _rating=extractRating("starApricot.gif");
       //System.out.println("_rating="+_rating);
       _classroomUse=extractClassroomUse();
       //System.out.println("_classroomUse="+_classroomUse);
       _writtenBy=extractAuthor();
       //System.out.println("_writtenBy="+_writtenBy);
       _authorID=extractAuthorID();
       //System.out.println("_authorID="+_authorID);
       _dateAdded=extractDateAdded();
       //System.out.println("_dateAdded="+_dateAdded);
       _remarks=extractRemarks();
       //System.out.println("_remarks="+_remarks);
       _technicalRemarks=extractTechnicalRemarks();
       //System.out.println("_technicalRemarks="+_technicalRemarks);
    }
    /**
     * Users HTMLParser class for getting the web code
     * @param url
     * @return web code from url
     */
    private String getCodeWebPage(String url)
    {
        HTMLParser Parser = new HTMLParser(url);
        return Parser.formatCode();
    }
    /**
     * Gets the rating
     * @param StringStar
     * @return rating
     */
    private int extractRating(String StringStar) {
        int rating=0;
        String StrStar=StringStar;
        try
        {
            int StarPosition=_code.indexOf(StrStar);
            while(StarPosition!=-1)
            {
                rating++;
                StarPosition=_code.indexOf(StrStar, StarPosition+1);
            }
        }
        catch(Exception e)
        {
          System.err.print("Error extracting Comment's Rating "+e.getMessage());
        }
        return rating;
    }
    
    private String extractMaterial(){
        String mat ="";
        String startString="viewMaterial";
        String secString="\">";
        String endString="</a>";
        int startPos = _code.indexOf(startString);
        if(startPos != -1){
            int secPos = _code.indexOf(secString, startPos);
            if(secPos != -1){
                int endPos = _code.indexOf(endString, secPos);
                mat = _code.substring(secPos+secString.length(), endPos);
            }
        }
        return mat;
    }
    
    private int extractMaterialID(){
        int matID=0;
        String firstStartString="viewMaterial.htm"; 
        int firstStartStringPos = _code.indexOf(firstStartString);
        String startString = "?id=";
        String endString="\">";
        int startPos = _code.indexOf(startString, firstStartStringPos);
        if(startPos != -1){
            int endPos = _code.indexOf(endString, startPos);
            if(endPos != -1){
                matID = Integer.parseInt(_code.substring(startPos+startString.length(), endPos));
            }
        }
        return matID;
    }
    
    public int getMaterialID(){
        return _materialID;
    }
    /**
     * Get classroom use
     * @return classroom use
     */
    private boolean extractClassroomUse() {
        /*String use="";
        String StartString="Classroom Use:";
        String EndString="Submitted by:";
        try
        {
            int StartPosition=_code.indexOf(StartString);
            int EndPosition=_code.indexOf(EndString,StartPosition);
            if(StartPosition!=-1 && EndPosition!=-1)
            {
             use=HTMLParser.deleteHTMLLabels
                     (_code, StartPosition+StartString.length(), EndPosition);
            }
        }
        catch(Exception e)
        {
            System.err.print("Error extracting Comment's classroom use "+
                    e.getMessage());
        }*/
        boolean use = true;
        if(_code.contains("Not used in course")) use = false;
        return use;
    }
    /**
     * Get the author name
     * @return author name
     */
    private String extractAuthor() {
       String author="";
       String StartString="Submitted by:";
       String EndString="</a>";
       try
       {
           int StartPosition=_code.indexOf(StartString);
           int EndPosition=_code.indexOf(EndString,StartPosition);
           if(StartPosition!=-1 && EndPosition!=-1)
           {
             author=HTMLParser.deleteHTMLLabels
                     (_code, StartPosition+StartString.length(), EndPosition);
           }
       }
       catch(Exception e)
        {
          System.err.print("Error extracting Comment's Author "+e.getMessage());
        }
       //System.out.println(author+"\n");
       return author;
    }
    /**
     * Gets the author ID
     * @return author ID
     */
    private int extractAuthorID() {
       String author="";
       String StartString="Submitted by:";
       String EndString="\">";
       int authorID=0;
       try
       {
           int StartPosition=_code.indexOf(StartString);
           int EndPosition=_code.indexOf(EndString,StartPosition);
           
           if(StartPosition!=-1 && EndPosition!=-1)
           {
             String NewStartString="?id=";
             int NewStartPosition=_code.indexOf(NewStartString,StartPosition);
             author=HTMLParser.deleteHTMLLabels
                     (_code, NewStartPosition+NewStartString.length(), EndPosition);
             //System.out.println("authorParseado="+author);
             authorID=Integer.parseInt(author);
           }
       }
       catch(Exception e)
        {
          System.err.print("Error extracting Comment's Author "+e.getMessage());
        }
       return authorID;
    }
    /**
     * Gets the date added
     * @return date added
     */
    private Date extractDateAdded()
    {
        Date DateAdded=new Date(0);
        String DateAdd="";
        //String StartString=_writtenBy.substring(_writtenBy.length()-3) + ", ";
        String StartString = _writtenBy+"</a>";
        //System.out.println("ss "+StartString+"\n");
        String EndString="</td>";
        try
        {
            int StartPosition=_code.indexOf(StartString);
            if(StartPosition!=-1)
            {
                int EndPosition=_code.indexOf(EndString,StartPosition);
                if(EndPosition!=-1)
                {   
                    String codeFragment = _code.substring(StartPosition, EndPosition);
                    int cut = codeFragment.indexOf(",");
                    
                    codeFragment = codeFragment.substring(cut+1);
                    DateAdd= codeFragment.trim();
                            //HTMLParser.deleteHTMLLabels
                             //(_code,StartPosition+StartString.length(), EndPosition);
                }
                else
                {
                    DateAdd=HTMLParser.deleteHTMLLabels(_code,
                            StartPosition+StartString.length(), StartPosition+StartString.length()+12);
                }
            }
            //System.out.println(DateAdd+"\n");
            //DateAdd = DateAdd.replaceAll("[^0123456789-]", "");
            DateAdded=HTMLParser.convertDate(DateAdd);
        }
        catch(Exception e)
        {
            System.err.print("Error extracting Comment's Date Added "+
                    e.getMessage());
        }

        return DateAdded;
    }
    /**
     * Gets the remarks
     * @return remarks
     */
    private String extractRemarks() {
        String remarks="";
        String StartString="Comment:";
        String EndString="</tr>";
        try
        {
            int StartPosition=_code.indexOf(StartString);
            int EndPosition=_code.indexOf(EndString,StartPosition);

            if(StartPosition!=-1 && EndPosition!=-1)
            {
             remarks=HTMLParser.deleteHTMLLabels
                     (_code, StartPosition+StartString.length(), EndPosition);
            }
            else if(StartPosition!=-1 && EndPosition==-1)
            {
               remarks=HTMLParser.deleteHTMLLabels
                    (_code, StartPosition+StartString.length(), _code.length());
            }
        }
        catch(Exception e)
        {
         System.err.print("Error extracting Comment's Remarks "+e.getMessage());
        }
        return remarks;
    }
    /**
     * Gets the technical remarks
     * @return technical remarks
     */
    private String extractTechnicalRemarks() {
        String technicalRemarks="";
        String StartString="Technical Remarks:";
        String EndString="</tr>";
        try
        {
            int StartPosition=_code.indexOf(StartString);
            int EndPosition=_code.indexOf(EndString, StartPosition);
            if(StartPosition!=-1)
            {
             technicalRemarks=HTMLParser.deleteHTMLLabels
                     (_code, StartPosition+StartString.length(), EndPosition);
            }
        }
        catch(Exception e)
        {
            System.err.print(this._idComment+"id\n");
            System.err.print("Error extracting Comment's technical remarks"+
                    e.getMessage());
        }
        return technicalRemarks;
    }

    /**
     * 
     * @return comment ID
     */
    public int getIdComment()
    {
        return _idComment;
    }
    /**
     *
     * @return material name
     */
    public String getMaterial()
    {
        return _material;
    }
    /**
     *
     * @return rating
     */
    public int getRating()
    {
        return _rating;
    }
    /**
     *
     * @return classroom use
     */
    public boolean getClassroomUse()
    {
        return _classroomUse;
    }
    /**
     *
     * @return written by
     */
    public String getWrittenBy()
    {
        return _writtenBy;
    }
    /**
     *
     * @return Date added
     */
    public Date getDateAdded()
    {
        return _dateAdded;
    }
    /**
     *
     * @return remarks
     */
    public String getRemarks()
    {
        return _remarks;
    }
    /**
     *
     * @return technical remarks
     */
    public String getTechnicalRemarks()
    {
        return _technicalRemarks;
    }
    /**
     *
     * @return author id
     */
    public int getAuthorID()
    {
        return _authorID;
    }

}
