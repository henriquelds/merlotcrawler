merlot-gatherer
===============

A library written in Java that scraps the MERLOT learning object repository (www.merlot.org). It stores all the information in a MySQL Server database that can be later used for analytics. Metrics crawler is still outdated.

If you want to cite this or know how this can be used for analytics please use this reference:


* Cechinel, C., Sánchez-Alonso, S. and García-Barriocanal, E. (2011). Statistical profiles of highly-rated learning objects. Comput. Educ. 57(1), pp. 1255-1269.

Created under LGPL license.


Author: Victor Apellaniz
Adapted by Henrique Lemos

Detailed information (about database instantiation and more) available on old version git page: https://github.com/ieru/merlot-gatherer/wiki

Obs.: Besides -onlyMaterials and -updateUsers options, option -onlyMembers was added.
Obs.2: If you want to run it, edit files lastpage.txt and lastpagem.txt putting only the number 1 in the first line, so the crawler will start from the beginning. Option -usersList (that gathers a desired group of users) needs a users IDs list to be inserted into config/config.xml containing the header tags <usersList> </usersList> and inside these tags each user ID inside the tags <userID> </userID>.